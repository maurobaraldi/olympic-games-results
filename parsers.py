from html.parser import HTMLParser

from json import loads
from utils import (
    load_html,
    request
)


class OlimpicGamesParser(HTMLParser):
    '''Parse the all olimpic (summer and winter) games.'''

    def __init__(self):
        HTMLParser.__init__(self)
        self.recording = 0
        self.games = []
        self.href = None

    def handle_starttag(self, tag, attrs):
        if tag != 'a':
            return
        if self.recording:
            self.recording += 1
            return
        for name, value in attrs:
            if name == 'href':
                self.href = value
                break
            else:
                return
        self.recording = 1

    def handle_endtag(self, tag):
        if tag == 'a' and self.recording:
            self.recording -= 1

    def handle_data(self, data):
        if self.recording and not data.startswith('\n'):
            self.games.append({"location": data[0:-5], 'year': int(data[-5::]), "url": self.href})

    def get_url_game(self, year, location):
        if year == location:
            return []
        if year is not None and location is not None:
            return [i.get('url') for i in self.games if i.get('year') == year and i.get('location') == location][0]


class OlimpicSportsParser(HTMLParser):
    '''Parse the all olimpic (summer and winter) games.'''

    def __init__(self):
        HTMLParser.__init__(self)
        self.recording = 0
        self.sports = None
        self.count = 0

    def handle_starttag(self, tag, attrs):
        if tag != 'script':
            return
        if self.recording:
            self.recording += 1
            return
        for name, value in attrs:
            if name == 'id' and value == '__NEXT_DATA__':
                self.count += 1
                break
            else:
                return
        self.recording = 1

    def handle_endtag(self, tag):
        if tag == 'script' and self.recording:
            self.recording -= 1

    def handle_data(self, data):
        if self.recording:
            if self.count == 1:
                #import pdb; pdb.set_trace()
                self.sports = loads(data).get('props').get('pageProps').get('olympicGame').get('disciplines')
                for s in self.sports:
                    s.pop('__typename')
                    s.pop('sportDisciplineId')
                    s['url'] = '/' + s['slug']
                    s.pop('slug')


class OlimpicResultsParser(HTMLParser):

    def __init__(self):
        HTMLParser.__init__(self)
        self.recording = 0
        self.discpline = None
        self._events = None
        self.count = 0

    def handle_starttag(self, tag, attrs):
        if tag != 'script':
            return
        if self.recording:
            self.recording += 1
            return
        for name, value in attrs:
            if name == 'id' and value == '__NEXT_DATA__':
                self.count += 1
                break
            else:
                return
        self.recording = 1

    def handle_endtag(self, tag):
        if tag == 'script' and self.recording:
            self.recording -= 1


    def handle_data(self, data):
        if self.recording:
            if self.count == 1:
                #import pdb; pdb.set_trace()
                self.discipline = loads(data).get('props').get('pageProps').get('discipline')
                self._events = loads(data).get('props').get('pageProps').get('gameDiscipline').get('events')
                for e in self._events:
                    e.pop('__typename')
                    e['url'] = '/' + e['slug']
                    e.pop('slug')

    @property
    def results(self):
        r = []
        for event in self._events:
            e = {}
            e['title'] = event.get(title)
            e['gender'] = event.get(gender)
            e['awards'] = []
            for award in event.get('awards'):
                a = {}
                a['medal'] = award.get('medalType')
                a['participant'] = award.get('participant').get('title')
                a['country'] = award.get('participant').get('country')
                a['athletes'] = award.get('participant').get('athletes')


