## URLs

base url: https://olympics.com/
olimpic_games: 
> https://olympics.com/en/olympic-games
> https://olympics.com/en/olympic-games/tokyo-2020

results: https://olympics.com/en/olympic-games/tokyo-2020/results/3x3-basketball

summary results (full)
https://olympics.com/en/olympic-games/tokyo-2020/results/3x3-basketball/men


## Games

The games comes from a partial html content from htmls datasets folder

Usage

```
from parsers import OlimpicGamesParser
from utils import (
    load_html,
    request
)

games = OlimpicGamesParser()
games.feed(load_html("olympic-games.html"))
games.close()
print(games.games)
games.get_url_game(1992)
games.get_url_game(1996)
```


## Sports list

This data can be scraped from https://olympics.com/en/olympic-games/tokyo-2020


```
from parsers import (
    OlimpicGamesParser,
    OlimpicSportsParser
)
from utils import (
    load_html,
    request
)

games = OlimpicGamesParser()
games.feed(load_html("olympic-games.html"))
games.close()
atlanta = request(games.get_url_game(1996, 'Atlanta')+'/results')
sports = OlimpicSportsParser()
sports.feed(atlanta)
print(sports.sports)
```

## Results

This data can be scraped from https://olympics.com/en/olympic-games/athens-1896/results/athletics

```
from parsers import (
    OlimpicGamesParser,
    OlimpicSportsParser,
    OlimpicResultsParser,
)
from utils import (
    load_html,
    request
)

games = OlimpicGamesParser()
games.feed(load_html("olympic-games.html"))
games.close()
atlanta = request(games.get_url_game(1996, 'Atlanta')+'/results')
sports = OlimpicSportsParser()
sports.feed(atlanta)

archery = OlimpicResultsParser()
archery_url = [s.get('url') for s in sports.sports if s.get('title') == 'Archery'][0]
results = request(games.get_url_game(1996, 'Atlanta')+'/results'+archery_url)
archery.feed(results)
```

### List of categories

<h2 class="styles__Title-sc-282810-2 bPUjCF">100m men</h2>

### Full results by categories

Link: <a data-cy="next-link" class="" href="/en/olympic-games/tokyo-2020/results/athletics/4-x-400m-relay-mixed">See full results</a>


### List of Countries (participants)

<span class="styles__CountryName-sc-rh9yz9-8 kWShpG">Poland</span>

### Athletes list 
<a data-cy="team-member" class="styles__AthleteLink-sc-12ivwwp-1 eomCYY" href="https://olympics.com/en/athletes/iga-baumgart"><span>iga baumgart</span><span data-cy="icon-arrow-right" class="Iconstyles__IconDefinitions-sc-1x3fvd7-1 eYkHMB"><i class="Iconstyles__Icon-sc-1x3fvd7-0 bmwsgp icon-arrow-right "></i></span></a>

Useful blocks of code in page

```
<div data-cy="country-name-row-1" class="styles__Country-sc-rh9yz9-9 iqcElo">
    <span class="styles__CountryName-sc-rh9yz9-8 kWShpG">Poland</span>
    <span data-cy="arrow-row-1" class="styles__IconWrapper-sc-rh9yz9-7 jPvlGz">
        <span data-cy="icon-caret-down" class="Iconstyles__IconDefinitions-sc-1x3fvd7-1 eYkHMB">
            <i class="Iconstyles__Icon-sc-1x3fvd7-0 bmwsgp icon-caret-down "></i>
        </span>
    </span>
</div>

<div data-cy="team-members-row-1" class="styles__TeamMembers-sc-rh9yz9-10 vhhLt">
    <div class="styles__Wrapper-sc-12ivwwp-0 gFAVtc">
        <a data-cy="team-member" class="styles__AthleteLink-sc-12ivwwp-1 eomCYY" href="https://olympics.com/en/athletes/iga-baumgart">
            <span>iga baumgart</span>
            <span data-cy="icon-arrow-right" class="Iconstyles__IconDefinitions-sc-1x3fvd7-1 eYkHMB">
                <i class="Iconstyles__Icon-sc-1x3fvd7-0 bmwsgp icon-arrow-right "></i>
            </span>
        </a>
        <a data-cy="team-member" class="styles__AthleteLink-sc-12ivwwp-1 eomCYY" href="https://olympics.com/en/athletes/kajetan-duszynski">
            <span>kajetan duszynski</span>
            <span data-cy="icon-arrow-right" class="Iconstyles__IconDefinitions-sc-1x3fvd7-1 eYkHMB">
                <i class="Iconstyles__Icon-sc-1x3fvd7-0 bmwsgp icon-arrow-right "></i>
            </span>
        </a>
        <a data-cy="team-member" class="styles__AthleteLink-sc-12ivwwp-1 eomCYY" href="https://olympics.com/en/athletes/malgorzata-holub">
            <span>malgorzata holub</span>
            <span data-cy="icon-arrow-right" class="Iconstyles__IconDefinitions-sc-1x3fvd7-1 eYkHMB">
                <i class="Iconstyles__Icon-sc-1x3fvd7-0 bmwsgp icon-arrow-right "></i>
            </span>
        </a>
        <a data-cy="team-member" class="styles__AthleteLink-sc-12ivwwp-1 eomCYY" href="https://olympics.com/en/athletes/natalia-kaczmarek">
            <span>natalia kaczmarek</span>
            <span data-cy="icon-arrow-right" class="Iconstyles__IconDefinitions-sc-1x3fvd7-1 eYkHMB">
                <i class="Iconstyles__Icon-sc-1x3fvd7-0 bmwsgp icon-arrow-right "></i>
            </span>
        </a>
        <a data-cy="team-member" class="styles__AthleteLink-sc-12ivwwp-1 eomCYY" href="https://olympics.com/en/athletes/dariusz-kowaluk">
            <span>dariusz kowaluk</span>
            <span data-cy="icon-arrow-right" class="Iconstyles__IconDefinitions-sc-1x3fvd7-1 eYkHMB">
                <i class="Iconstyles__Icon-sc-1x3fvd7-0 bmwsgp icon-arrow-right "></i>
            </span>
        </a>
        <a data-cy="team-member" class="styles__AthleteLink-sc-12ivwwp-1 eomCYY" href="https://olympics.com/en/athletes/justyna-swiety">
            <span>justyna swiety</span>
            <span data-cy="icon-arrow-right" class="Iconstyles__IconDefinitions-sc-1x3fvd7-1 eYkHMB">
                <i class="Iconstyles__Icon-sc-1x3fvd7-0 bmwsgp icon-arrow-right "></i>
            </span>
        </a>
        <a data-cy="team-member" class="styles__AthleteLink-sc-12ivwwp-1 eomCYY" href="https://olympics.com/en/athletes/karol-zalewski">
            <span>karol zalewski</span>
            <span data-cy="icon-arrow-right" class="Iconstyles__IconDefinitions-sc-1x3fvd7-1 eYkHMB">
                <i class="Iconstyles__Icon-sc-1x3fvd7-0 bmwsgp icon-arrow-right "></i>
            </span>
        </a>
    </div>
</div>
```