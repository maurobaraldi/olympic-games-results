from os.path import (
    abspath,
    curdir,
    join
)
from urllib.request import urlopen

def request(url: str) -> str:
    '''Request uri and return it as data (str).'''

    if "https://olympics.com" not in url:
        url = "https://olympics.com" + url
    print(url)

    with urlopen(url) as r:
        return r.read().decode('utf8')

def load_html(filename: str) -> str:
    '''Open HTML data and return it as data (str).'''

    path = join(abspath(curdir), 'htmls', filename)

    with open(path) as f:
        return f.read()
